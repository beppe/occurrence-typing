Gradual typing is an approach proposed by~\citet{siek2006gradual} to
combine the safety guarantees of static typing with the programming
flexibility of dynamic typing. The idea is to introduce an \emph{unknown} 
(or \emph{dynamic}) type, denoted $\dyn$, used to inform the compiler that
some static type-checking can be omitted, at the cost of some additional
runtime checks. The use of both static typing and dynamic typing in a same
program creates a boundary between the two, where the compiler automatically
adds---often costly~\cite{takikawa2016sound}---dynamic type-checks to ensure 
that a value crossing the barrier is correctly typed.

Occurrence typing and gradual typing are two complementary disciplines
which have a lot to gain to be integrated, although we are not aware
of any study in this sense. We explore this integration for the
formalism of Section~\ref{sec:language} for which the integration of
gradual typing was first defined by~\citet{CL17} and successively considerably
improved by~\citet{castagna2019gradual} (see~\citet{Lanvin21phd} for a
comprehensive presentation).

In a sense, occurrence typing is a
discipline designed to push forward the frontiers beyond which gradual
typing is needed, thus reducing the amount of runtime checks needed. For 
instance, the JavaScript code of~\eqref{foo} and~\eqref{foo2} in the introduction can also be
typed by using gradual typing:%\svvspace{-.2mm}
\begin{alltt}\color{darkblue}\morecompact
  function foo(x\textcolor{darkred}{ : \pmb{\dyn}}) \{
      return (typeof(x) === "number")? x+1 : x.trim();  \refstepcounter{equation}                                \mbox{\color{black}\rm(\theequation)}\label{foo3}
  \}\negspace
\end{alltt}
``Standard'' or ``safe'' gradual typing inserts two dynamic checks since it compiles the code above into:
\begin{alltt}\color{darkblue}\morecompact
  function foo(x) \{
      return (typeof(x) === "number")? (\textcolor{darkred}{\Cast{number}{{\color{darkblue}x}}})+1 : (\textcolor{darkred}{\Cast{string}{{\color{darkblue}x}}}).trim();
  \}\negspace
\end{alltt}
where {\Cast{$t$}{$e$}} is a type-cast that dynamically checks whether the value returned by $e$ has type $t$.\footnote{Intuitively, \code{\Cast{$t$}{$e$}} is
  syntactic sugar for \code{(typeof($e$)==="$t$")\,?\,$e$\,:\,(throw "Type
    error")}. Not exactly though, since to implement compilation \emph{à la} sound gradual typing it is necessary to use casts on function types that need special handling.}
%
We already saw  that thanks to occurrence typing we can annotate the parameter \code{x} by \code{number|string} instead of \dyn{} and avoid the insertion of any cast. 
But occurrence typing can be used also on the gradually typed code above in order to statically detect the insertion of useless casts. Using
occurrence typing to type the gradually-typed version of \code{foo} in~\eqref{foo3}, allows the system to avoid inserting the first cast
\code{\Cast{number}{x}} since, thanks to occurrence typing, the
occurrence of \code{x} at issue is given type \code{number} (but the
second cast is still necessary though). But removing only this cast is far
from being satisfactory, since when this function is applied to an integer
there are some casts that still need to be inserted outside  the function.
The reason is that the compiled version of the function
has type \code{\dyn$\to$number}, that is, it expects an argument of type
\dyn, and thus we have to apply a cast (either to the argument or
to the function) whenever this is not the case. In particular, the
application \code{foo(42)} will be compiled as
\code{foo(\Cast{\dyn}{42})}. Now, the main problem with such a cast is not
that it produces some unnecessary overhead by performing useless
checks (a cast to \dyn{} can easily be detected and safely ignored at runtime). 
The main problem is that the combination of such a cast with type-cases 
will lead to unintuitive results under the standard operational
semantics of type-cases and casts.
Indeed, consider the standard semantics
of the type-case \code{(typeof($e$)==="$t$")} which consists in
reducing $e$ to a value and checking whether the type of the value is a
subtype of $t$. In standard gradual semantics, \code{\Cast{\dyn}{42}} is a value. 
And this value is of type \code{\dyn}, which is not a subtype of \code{number}. 
Therefore the check in \code{foo} would fail for \code{\Cast{\dyn}{42}}, and so
would the whole function call.
Although this behavior is type safe, this violates the gradual 
guarantee~\cite{siek2015refined} since giving a \emph{more precise} type to
the parameter \code{x} (such as \code{number}) would make the function succeed,
as the cast to \code{$\dyn$} would not be inserted.
A solution is to modify the semantics of type-cases, and in particular of 
\code{typeof}, to strip off all the casts in values, even nested ones.
While this adds a new overhead at runtime, this is preferable to losing the
gradual guarantee, and the overhead can be mitigated by having a proper
representation of cast values that allows to strip all casts at once.

However, this problem gets much more complex when considering functional values.
In fact, as we hinted in Section~\ref{ssec:algorithm}, there is no way to
modify the semantics of type cases to preserve both the gradual guarantee and
the soundness of the system in the presence of arbitrary type cases.
For example, consider the function
$f = \lambda^{(\Int \to \Int) \to \Int} g. \tcase{g}{(\Int \to \Int)}{g\
1}{\code{true}}$. This function is well-typed since the type of the
parameter guarantees that only the first branch can be taken, and thus that
only an integer can be returned. However, if we apply this function
to $h = \MCast{\Int \to \Int}{(\lambda^{\dyn \to \dyn} x.\ x)}$, the type case
strips off the cast around $h$ (to preserve the gradual guarantee), then
checks if $\lambda^{\dyn \to \dyn} x.\ x$ has type $\Int \to \Int$. 
Since $\dyn \to \dyn$ is not a subtype of $\Int \to \Int$, the check fails
and the application returns $\code{true}$, which is unsound.
Therefore, to preserve soundness in the presence of gradual types, type cases
should not test functional types other than $\Empty \to \Any$, which is
the same restriction as the one presented by~\citet{siek2016recursive}.

While this solves the problem of the gradual guarantee, it is clear that
it would be much better if the application \code{foo(42)} were compiled as is,
without introducing the cast \code{\Cast{\dyn}{42}}, thus getting rid of the
overhead associated with removing this cast in the type case.
This is where the previous section about refining function types comes in handy.
To get rid of all superfluous casts, we have to fully exploit the information 
provided to us by occurrence typing and deduce for the function in~\eqref{foo3} the type
\code{(number$\to$number)$\wedge$((\dyn\textbackslash
  number)$\to$string)}, so that no cast is inserted when the
function is applied to a number. 
To achieve this, we simply modify the typing rule for functions that we defined
in the previous section to accommodate for gradual typing. Let $\sigma$ and $\tau$ range over \emph{gradual types}, that is the types produced by the grammar in Definition~\ref{def:types} to which we add \dyn{} as basic type (see~\citet{castagna2019gradual} for the definition of the subtyping relation on these types). For every gradual type
$\tau$, define $\tauUp$ as the (non gradual) type obtained from $\tau$ by replacing all
covariant occurrences of \dyn{} by \Any{} and all contravariant ones by \Empty. The
type $\tauUp$ can be seen as the \emph{maximal} interpretation of $\tau$, that is,
every expression that can safely be cast to $\tau$ is of type $\tauUp$. In
other words, if a function expects an argument of type $\tau$ but can be 
typed under the hypothesis that the argument has type $\tauUp$, then no casts
are needed, since every cast that succeeds will be a subtype of
$\tauUp$. Taking advantage of this property, we modify the rule for
functions as: \svvspace{-2mm}
%
%\begin{mathpar}
%  \Infer[Abs]
%    {\Gamma,x:\tau\vdash e\triangleright\psi\and \forall i\in I\quad \Gamma,x:\sigma_i\vdash e:\tau_i
%     \and \forall j \in J \subseteq I\quad \Gamma,x:\sigma_j^*\vdash e:\tau_j}
%    {
%    \Gamma\vdash\lambda x:\tau.e:\textstyle\bigwedge_{i\in I}\sigma_i\to \tau_i
%      \land \bigwedge_{j\in J}\sigma_j^*\to \tau_j
%    }
%    {\psi(x)=\{\sigma_i\alt i\in I\}}
%\end{mathpar}
\[
  \textsc{[AbsInf+]}
  \frac
  {
    \begin{array}{c}
    \hspace*{-8mm}T = \{ (\sigma', \tau') \} \cup \{ (\sigma,\tau) ~|~ \sigma \in \psi(x) \land \Gamma, x: \sigma \vdash e: \tau \} \cup \{ (\sigmaUp,\tau) ~|~ \sigma \in \psi(x) \land \Gamma, x: \sigmaUp \vdash e: \tau \}\\
    \Gamma,x:\sigma'\vdash e\triangleright\psi \qquad \qquad \qquad\Gamma,x:\sigma'\vdash e:\tau'
    \end{array}
  }
  {
    \Gamma\vdash\lambda x:\sigma'.e:\textstyle\bigwedge_{(\sigma,\tau) \in T}\sigma\to \tau
  }\svvspace{-2mm}
\]
The main idea behind this rule is the same as before: we first collect all the
information we can into $\psi$ by analyzing the body of the function. We then
retype the function using the new hypothesis $x : \sigma$ for every 
$\sigma \in \psi(x)$. Furthermore, we also retype the function using the hypothesis
$x : \sigmaUp$: as explained before the rule, whenever this typing succeeds it eliminates unnecessary gradual types and, thus, unecessary casts.
Let us see how this works on the function \code{foo} in \eqref{foo3}. First, we deduce
the refined hypothesis 
$\psi(\code x) = \{\,\code{number}{\land}\dyn\;,\;\dyn \textbackslash \code{number}\,\}$.
Typing the function using this new hypothesis but without considering the
maximal interpretation would yield
$(\dyn \to \code{number}\vee\code{string}) \land ((\code{number} \land \dyn) \to \code{number})
\land ((\dyn \textbackslash \code{number}) \to \code{string})$. However, as
we stated before, this would introduce an unnecessary cast if the function 
were to be applied to an integer.\footnote{%
Notice that considering
$\code{number} \land \dyn\simeq \code{number}$ is
not an option, since it would force us to choose between having
the gradual guarantee or having, say,
$\code{number} \land \code{string}$ be more precise than
$\code{number} \land \dyn$.\svvspace{-2mm}}
Hence the need for the second part of
Rule~\textsc{[AbsInf+]}: the maximal interpretation of $\code{number} \land \dyn$
is $\code{number}$, and it is clear that, if $\code x$ is given type \code{number},
the function type-checks, thanks to occurrence typing. Thus, after some
routine simplifications, we can actually deduce the desired type
$(\code{number} \to \code{number}) \land ((\dyn \textbackslash \code{number}) \to \code{string})$.



\beppe{Problem: how to compile functions with intersection types, since their body is typed several distinct types. I see two possible solutions: either merge the casts of the various typings (as we did in the compilation of polymorphic functions for semantic subtyping) or allow just one gradual type in the intersection when a function is explicitly typed (reasonable since, why would you use more gradual types in an intersection?)}
