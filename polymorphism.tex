
TODO: update rules (add Empty rule, etc.)

\input{roadmappolymorphism}

\beppe{Several open PROBLEMS:
%
First, how to deal with generic expressions, since we want to deduce from $x_1(x_1 x_2)\in\Int$ that $x_2:\Int$ in the then branch if $x_1:\alpha\to\alpha$.
%
Second, how to extend the work to union of intersection of arrows?
Shall we just work with domain and result types or rather specialize
each arrow of the intersection accordingly?  }

So let us syntactically distinguish the polymorphic function from
monomorphic ones.


\subsection{Polymorphism without arrow intersections}

To formally study the problem of occurrence typing for polymorphic
languages we use an explicitly-typed language with let polymorphims
and explicit type instantiation. For presentation purposes, we
introduce it in two steps.  First we consider a simpler version in
which functions cannot be assigned intersection of arrow types (unless
by subsumption) and, thus, they are typed just a single, possibly
polymorphic arrow type. Then in the next section we extend the
calculus to cope with functions annotated with intersection types (as
in the previous part) which requires a lot of more technical
machinery. Of course neither of the calculi presented is a language to
program with but rather they are intermediate representations in which
one want to compile to. As a matter of facts the version we introduce
in the next section is an idealized version of the intermediate
representation of the polymorphic version of the CDuce language and it
is directly inspired by the intermediate language defined
by~\citet{polyduce1} and by the cast calculus presented
by~\citet{castagna2019gradual}.
\[
\begin{array}{lrcl}
\textbf{Types} & t & ::= & \alpha\alt b\alt t\to t\alt t\vee t \alt \neg t \alt \Empty\\[3mm] 
  \textbf{Exprs} & e & ::= & c\alt x \alt \lambda^{t\to t}x.e \alt \ite e t e e \alt e e
  \alt \Lambda\vec\alpha.e\alt e[t] \alt \Let x e e
\end{array}
\]

We have besides the classic $\beta$ the following new reduction rule. 
\[(\Lambda\vec\alpha.e)[\vec t]\leadsto e\{\vec\alpha\mapsto\vec t\}\]

Now $\Gamma$ maps expressions into schemas of the form
$\forall\vec\alpha.t$. A type is just a trivial schema where the
vector is empty. In reality just variables can be mapped to
non-trivial schemas. We outline the [Var] rule so as to stress all the
places in which the inference system may deduce schemas instead of
just simple types.



\begin{mathpar}

\Infer[Var]
    {
    }
    { \Gamma \vdash x: \forall\vec\alpha.t }
    { \Gamma(x)=\forall\vec\alpha.t}
\qquad
\Infer[Occ]
    {
    }
    { \Gamma \vdash e: \Gamma(e) }
    { e\in\dom\Gamma\setminus \textsf{Vars}}
\qquad
\Infer[Const]
    { }
    {\Gamma\vdash c:\basic{c}}
    {c\not\in\dom\Gamma}
 \\
\Infer[Tabs]
   {\Gamma \vdash e:t}
   {\Gamma \vdash \Lambda\vec\alpha.e:\forall\vec\alpha.t}
   {\vec\alpha\#\Gamma}
   \qquad
\Infer[Tapp]
   {\Gamma \vdash  e:\forall\vec\alpha.t}
   {\Gamma  \vdash e[\vec t]:t\{\vec\alpha\mapsto\vec t\}}
   {}
\\
\Infer[Abs]
    {\Gamma,x:s_i\vdash e:t_i'\\t_i'\leq t_i}
    {
    \Gamma\vdash\lambda^{s\to t}x.e:s\to t
    }
    {\lambda^{s\to t}x.e\not\in\dom\Gamma}
    \\
\Infer[App]
    {
      \Gamma \vdash e_1: t_1 \to t \\
      \Gamma\vdash e_2: t_2\\
      t_2\leq t_1
    }
    { \Gamma \vdash {e_1}{e_2}: t }
    { {e_1}{e_2}\not\in\dom\Gamma}
    \\
\Infer[If]
      {\Gamma\vdash e:t_\circ\\
        \Gamma, \Gamma^+_{\Gamma,e,t}\vdash e_1 : t_1\\
        \Gamma, \Gamma^-_{\Gamma,e,t}\vdash e_2 : t_2}
      {\Gamma\vdash \ite {e} t {e_1}{e_2}: t_1\vee t_2}
      {\makebox[3cm][l]{$\ite {e} t {e_1}{e_2}\not\in\dom\Gamma$}}\\
\Infer[Let]
      {\Gamma\vdash e_1:\forall\vec\alpha.t_1\\
        \Gamma, x:\forall\vec\alpha.t_1\vdash e_2 : t}
      {\Gamma\vdash \Let{x}{e_1}{e_2}: t}
      {\makebox[3cm][l]{$\alpha\#\Gamma,\Let{x}{e_1}{e_2}\not\in\dom\Gamma$}}

\\\end{mathpar}


\subsection{Polymorphism with arrow intersections}



We use the following syntax we enrich our
expressions with type abstractions, type instantiation and sets of
type-substitutions that index $\lambda$-abstractions. Notation: a type
substitution, ranged over by $\sigma$ is a finite mapping from type
variable to types, noted $\{\alpha_1\mapsto t_1, ..., \alpha_n\mapsto
t_n\}$. We will often use the more compact notation
$\{\vec\alpha\mapsto\vec t\}$ implying that $\vec\alpha$ and $\vec t$
are vectors of the same length. We will also use collections of type substitutions $[\sigma_i]_{i\in I}$ more compactly noted as $\sigma_I$, and collections of type vectors, noted as $[\{\vec t_i\}]_{i\in I}$ more compactly noted as $[\vec t]_I$
We then have the following syntax. 
\[
\begin{array}{lrcl}
\textbf{Types} & t & ::= & \alpha\alt b\alt t\to t\alt t\vee t \alt \neg t \alt \Empty\\[3mm] 
  \textbf{Exprs} & e & ::= & c\alt x \alt \lambda^{\wedge_{i\in I}s_i\to t_i}_{\sigma_I}x.e \alt \ite e t e e \alt e e
  \alt \Lambda\vec\alpha.e\alt e[\vec t]_I \alt \Let x e e
\end{array}
\]
This is necessary to combine instantiation with intersection
types. See~\citet{polyduce1} but in a nutshell. In this system the
polymorphic identity $\lambda x.x:\alpha\to\alpha$ is written as
$\Lambda \alpha.\lambda^{\alpha\to\alpha}x.x$. If I want to apply it
to an integer I have first to instantiate $\alpha$ to \Int{} as in
$(\Lambda \alpha.\lambda^{\alpha\to\alpha}x.x)[\Int]42$, which reduces
in one step to $(\lambda^{\Int\to\Int}x.x)42$. By instantiation
$\lambda x.x:\alpha\to\alpha$ has both type $\Int\to\Int$ and type
$\Bool\to\Bool$ so it has its intersection:
$(\Int\to\Int)\wedge(\Bool\to\Bool)$. So how do we obtain the instance
of this type? We instantiate $\alpha$ twice, once for \Int{} and once
for \Bool{}, by using the following syntax
$(\Lambda \alpha.\lambda^{\alpha\to\alpha}x.x)[\{\Int\},\{\Bool\}]$
which reduces to
$\lambda^{(\Int\to\Int)\wedge(\Bool\to\Bool)}x.x$. Technically, what we
have done it to apply the collection of type substitution
$[\{\alpha\mapsto\Int\},\{\alpha\mapsto\Bool\}]$ to the type
$\alpha\to\alpha$ of the $\lambda$-abstraction since we define
$t[\sigma_i]_{i\in I}$ as $\bigwedge_{i\in I}t\sigma_i$. We extend this definition to vectors by defining $\vec t\sigma_I\eqdef t_1\sigma_I,...,t_n\sigma_I$ for $\vec t = t_1,...,t_n$.

For technical reasons explained~\citet{polyduce1} it is not
possible to propagate sets of type substitutions below lambdas, and we
have to keep the sets of substitution as an extra component of lambda
abstractions. So actually
$(\Lambda \alpha.\lambda^{\alpha\to\alpha}x.x)[\{\Int\},\{\Bool\}]$
reduces to
$\lambda_{[\{\alpha\mapsto\Int\},\{\alpha\mapsto\Bool\}]}^{\alpha\to\alpha}x.x$. This is formalized by the following reduction rules.

\[(\Lambda\vec\alpha.e)[\{\vec t_i\}]_{i\in I}\leadsto e@[\{\vec\alpha\mapsto\vec t_i\}]_{i\in I}\]

where ``@'' is a relabeling operation takes an expression $e$ and a set of type-substitutions $\sigma_I$ and 
pushes $\sigma_I$ to all outermost $\lambda$-abstractions occurring in
$e$, and is defined as follows
\begin{eqnarray*}
x@\sigma_I & \eqdef & x\\
(\lambda^t_{\sigma_J}x.e)@\sigma_I & \eqdef &  \lambda^t_{\sigma_I\circ\sigma_J}x.e\\
(e_1e_2)@\sigma_I & \eqdef & (e_1@\sigma_I)(e_2@\sigma_I)\\
(\ite e t {e_1}{e_2})@\sigma_I& \eqdef & (\ite {e@\sigma_I} t {e_1@\sigma_I}{e_2@\sigma_I})\\
(\Lambda\vec\alpha.e)@\sigma_I& \eqdef &\Lambda\vec\alpha.(e@\sigma_I)\hspace*{12mm}(\vec\alpha\#\sigma_I)\\
(e[\{\vec t_j\}]_{j\in J})@\sigma_I& \eqdef & (e@\sigma_I)[\{\vec{t'_j}\}]_{j\in J}\qquad \text{ with } \vec{t'_j}=\vec{t_j}\sigma_I\\
(e[\{\vec t_j\}]_{j\in J})@\sigma_I& \eqdef & (e@\sigma_I)[\{\vec{t'_j\sigma_i}\}]_{i\in I,j\in J}\qquad \text{ where } \sigma_I=[\sigma_i]_{i\in I}
\end{eqnarray*}
\beppe{Not sure about the last definition: intuitively we have to compose the two substitutions (my first version was the second one)}
Collections of type substitutions in a lambda are applied at the moment of the application of the function by the following generalization of the $\beta$ rule
\[ \hspace*{2cm}\lambda^{\wedge_{i\in I}s_i\to t_i}_{\sigma_J}x.e\leadsto e@[\sigma_j]_{j\in P}\{x:= v\} \qquad\text{ where } P=\{j\in J\alt \exists i\in I, \vdash v:t_i\sigma_j\}\]


Now $\Gamma$ maps expressions into schemas of the form
$\forall\vec\alpha.t$. A type is just a trivial schema where the
vector is empty. In reality just variables can be mapped to
non-trivial schemas. We outline the [Var] rule so as to stress all the
places in which the inference system may deduce schemas instead of
just simple types.


\begin{mathpar}

\Infer[Var]
    {
    }
    { \Gamma \vdash x: \forall\vec\alpha.t }
    { \Gamma(x)=\forall\vec\alpha.t}
\qquad
\Infer[Occ]
    {
    }
    { \Gamma \vdash e: \Gamma(e) }
    { e\in\dom\Gamma\setminus \textsf{Vars}}
\qquad
\Infer[Const]
    { }
    {\Gamma\vdash c:\basic{c}}
    {c\not\in\dom\Gamma}
 \\
\Infer[Tabs]
   {\Gamma \vdash e:t}
   {\Gamma \vdash \Lambda\vec\alpha.e:\forall\vec\alpha.t}
   {\vec\alpha\#\Gamma}
   \qquad
\Infer[Tapp]
   {\Gamma \vdash  e:\forall\vec\alpha.t}
   {\Gamma  \vdash e[\{\vec t_i\}]_{i\in I}:t[\{\vec\alpha\mapsto\vec t_i\}]_{i\in I}}
   {}
\\
\Infer[Abs]
    {\Gamma,x:s_i\sigma_j\vdash e@[\sigma_j]:t_i'\sigma_j\\t_i'\sigma_j\leq t_i\sigma_j}
    {
    \Gamma\vdash\lambda^{\wedge_{i\in I}s_i\to t_i}_{[\sigma_j]_{j\in J}}x.e:\textstyle\bigwedge_{i\in I,j\in J}s_i\sigma_j\to t_i\sigma_j
    }
    {\lambda^{\wedge_{i\in I}s_i\to t_i}_{[\sigma_j]_{j\in J}}.e\not\in\dom\Gamma}
    \\
\Infer[App]
    {
      \Gamma \vdash e_1: t_1 \\
      \Gamma\vdash e_2: t_2\\
      t_1 \leq \Empty \to \Any\\
      t_2 \leq \dom {t_1}
    }
    { \Gamma \vdash {e_1}{e_2}: t_1 \circ t_2 }
    { {e_1}{e_2}\not\in\dom\Gamma}
    \\
\Infer[If]
      {\Gamma\vdash e:t_\circ\\
        \Gamma, \Gamma^+_{\Gamma,e,t}\vdash e_1 : t_1\\
        \Gamma, \Gamma^-_{\Gamma,e,t}\vdash e_2 : t_2}
      {\Gamma\vdash \ite {e} t {e_1}{e_2}: t_1\vee t_2}
      {\makebox[3cm][l]{$\ite {e} t {e_1}{e_2}\not\in\dom\Gamma$}}\\
\Infer[Let]
      {\Gamma\vdash e_1:\forall\vec\alpha.t_1\\
        \Gamma, x:\forall\vec\alpha.t_1\vdash e_2 : t}
      {\Gamma\vdash \Let{x}{e_1}{e_2}: t}
      {\makebox[3cm][l]{$\alpha\#\Gamma,\Let{x}{e_1}{e_2}\not\in\dom\Gamma$}}

\\\end{mathpar}
A word on implementation. This second calculus can be actually
efficiently implemented since, as explained by~\citet{polyduce1} it is
not necessary to apply at run-time the relabeling operation ``@''
apart from very specific and rare cases (in a nutshell, when a
typecase is performed on a partial application of a currified
function) that can be statically detected and optimized. If
furthermore we forbid to dynamically check the precise type of a
function and allow only checking whether it is a function or not
(i.e., in the type case we allow to check only the $\Empty\to\Any$
type), then relabelling is useless at runtime and, thus, it becomes
only a technical tool to prove subject reduction.
