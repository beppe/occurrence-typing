/** install the tsc compiler (through a package or with npm) and
    transpile to js with tsc --strict examples.ts
    then open the file examples.html in chrome or firefox and open the
    JS console to test the functions
    **/
enum ENil  { NIL };
type EList = ENil | { node : ENode; next : EList };

type EDocument = { kind : 1 };
type EElement = { kind : 2; children : EList};
type EText = { kind : 9; isEmpty : () => boolean };
type ENode = EDocument | EElement | EText;


function isEmpty3 (x : EDocument) : true;
function isEmpty3 (x : { kind : 2; children : ENil }) : true;
function isEmpty3 (x : { kind : 2; children : { node : ENode; next : EList } }) : false;
/* 
 without this case the last application fails to type-check 
 this corresponds to explicitly adding the case for the union of
 the two previous declarations
 */
function isEmpty3 (x : EElement) : boolean;
function isEmpty3 (x : EText) : boolean;
function isEmpty3 (x : ENode) : boolean {
    if (x.kind == 1) {
        return true;
    } else if (x.kind == 2 && x.children == ENil.NIL) {
        return true;
    } else if (x.kind == 2) {
        return false;
    } else  {
        return x.isEmpty();
    }
}

isEmpty3({ kind : 2, children : ENil.NIL });

function foo ():EList {
    if (Math.random()< .5){
        return ENil.NIL
    } else {
        return {node: {kind : 1}, next: ENil.NIL }
    }
}

isEmpty3({ kind : 2, children : foo() });

function or_ (x : true, y : boolean ) : true ;
function or_ (x : false, y : true) : true;
function or_ (x : false, y : false) : false;
function or_ (x : boolean, y : boolean) : boolean {
 return x || y;
}

let nn : false = or_(false, false);

function not_ (x : true) : false;
function not_ (x : false) : true;
function not_ (x : boolean) : boolean {
    return !x;
}

function and_ (x : false, y : boolean ) : false ;
function and_ (x : true, y : true) : true;
function and_ (x : true, y : false) : false;
function and_ (x : boolean, y : boolean) : boolean {
 return x || y;
};

let l : false = and_(false, true);
console.log(l);

