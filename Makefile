default:
	latexmk -pdf -bibtex main-elsarticle.tex




clean:
	rm -rf *.synctex* *.blg *.dvi *.aux *.fdb_latexmk *.log *.fls *.xcp *.out
