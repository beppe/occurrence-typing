
Extending our work to the case of a polymorphic language is far from
trivial. Let us go back to our typical example
expression~\eqref{typical} of the introduction:
\begin{equation}\label{typagain}
\ifty{x_1x_2}{t}{e_1}{e_2}
\end{equation}
we have seen that occurrence typing for $x_1$ and $x_2$ was possible
only in very specific cases which depended on the form of the type of
$x_1$: $(1)$ the type of $x_2$ may be specialized only if the type of $x_1$
is an intersection of arrows and $(2)$ the type of $x_1$ may be specialized
only if the type of $x_1$ is a union of arrows. With polymorphic types
the first assertion is, strictly speaking, no longer true. The
simplest possible example to show it, is when $x_1$ is of type $\alpha\to\alpha$
and $x_2$ is of type $\alpha$. Then it is clear that we can assume
that $x_2$ has type $t$ when typing $e_1$ and that it has type $\neg
t$ when typing $e_2$.


The deduction becomes much more difficult when one adds subtyping and
set-theoretic types to the game. Let us consider a couple of more examples to
finger the key cases.

Take again the expression in~\eqref{typagain} and imagine that $t$ is
\Int{} and that $x_1$ has type $\alpha\to
(\alpha\vee\Bool)$.\footnote{%
  A non-trivial example of an expression
  of this type is the function $\lambda^{\alpha\to
    (\alpha\vee\Bool)}x. \ite e u x \texttt{true}$. for some expression $e$ and type $u$.}
%
We suppose the expression $x_1x_2$ to be well-typed and
therefore that $x_2$ is typed by a subtype of $\alpha$, say, 
$\alpha\wedge t_\circ$. The case for the ``then'' is not very
different from the previous one when $x_1$ had type $\alpha\to\alpha$:
the application  $x_1x_2$ has type $\alpha\vee\Bool$ so if the test  $x_1x_2\in\Int$ succeeds, then it
is because the value yielded by the application is an integer and
this integer must come from the $\alpha$ summand of the union. Since
we do not know exactly which integer we may obtain, we include all of them
in $\alpha$, which yields \Int{} as the best possible approximation for
$\alpha$. So when typing $e_1$ we can safely assume that $x_2$ has
type \Int{} ---or, more precisely, $\Int\wedge t_\circ$, since we use the static type information about $x_2$---. The case for
$e_2$, instead, is different since assuming that $x_2$ has type
$\neg\Int$ (more precisely, $\neg\Int\wedge t_\circ$) would be unsound,
insofar as the check may fail because the application returned a
Boolean, which can happen even when $x_2$ is bound to an
integer. Therefore, when typing $e_2$ we cannot specialize the type of
$x_2$ which must thus be assumed to be $\alpha\wedge t_\circ$.

For a concrete example of why this would be unsound take $t_\circ=\Int\vee\Bool$ and consider
\[
\ifty{x_1x_2}{\Int}{x_2+1}{\texttt{not($x_2$)}}
\]
with $x_1:\alpha\to (\alpha\vee\Bool)$ and
$x_2:\alpha\wedge(\Int\vee\Bool)$. If when typing the else branch we
assume that $x_2$ has type $\neg\Int\wedge t_\circ$, that is, $\Bool$,
then the above expression would be well typed. But at run time $x_1$
can be bound to the constant function that always returns \texttt{true},
$\lambda^{\alpha\to\Bool} x. \texttt{true}$ (which by subsumption is
of type $\alpha\to (\alpha\vee\Bool)$) and $x_2$ to an integer, say,
\texttt{42} which would reduce to the expression \texttt{not(42)},
which is not well typed.


As a final example consider the case in which $x_1:\alpha\to
(\alpha\vee\texttt{true})$, $x_2:\alpha\wedge t_\circ$, and
$t=\Bool$. This case is somehow the dual of the previous one. For the
case ``then'' we cannot do any further assumption on the type of
$x_2$, since the test may have succeeded because the application
returned \texttt{true} and this may happen independently from the type
of $x_2$. For the ``else'' case instead we can safely assume that the
check failed due to the $\alpha$ part of the result, and therefore when typing $e_2$ we
can safely assume that $x_2$ has type $x_2:\neg\Bool\wedge t_\circ$.







The reason why polymorphism makes a difference is that, intuitively, a
polymorphic function type already is an intersection of arrows,
insofar as from an observational point of view it is equivalent
to the infinite intersection of all its instances. Since we cannot work
with infinitely many instances, we will pick up those that give us
the information we need for occurrence typing and that are computed
starting from the type of $x_1$ and from the type $t$ as we explain next.

The idea is to single out the two most general type substitutions for
which some test may succeed and fail, respectively, and apply these substitutions to
refine the types of the corresponding occurrences when typing the
``then'' and ``else'' branches.


Consider:
\begin{itemize}
\item $x_1: s\to t$
\item $x_2: u$ with $u\leq s$
\item the test $x_1x_2\in \tau$ where $\tau$ is a closed type.
\end{itemize}
Then we proceed as follows for the THEN branch:\\


First, check whether $\exists \sigma$ such that $t\sigma\leq\neg\tau$.

\begin{itemize}
\item 
  If such a $\sigma$ does not exist, then this means that for all
  possible assignments of polymorphic type variables of $s\to t$, the
  test may succeed. Therefore the success of the test does not depend
  on the particular instance of $s\to t$ and, thus, it is not possible
  to pick some substitution that differentiates the success of the
  test and that could specialize the type of $x_2$ in the ``then''
  branch.
\item If $\exists \sigma$ such that $t\sigma\leq\neg\tau$, then we
  know that there is at least one assignment for the type variables of
  $s\to t$ that ensures that the test cannot but fail. Therefore for the
  typing of the branch ``then'' we want to exclude all such
  substitutions. To put it otherwise we want only to pick the
  substitutions $\sigma$ for which the intersection of $\tau$ and
  $t\sigma$ is not empty, that is, that there is a value in $\tau$
  that may be the result of the application. These are infinitely many
  substitutions (if $\tau$ contains infinitely many values), and since
  we do not know which one will be used (in the case of success, we
  just know that at least one of them will be used but not which one),
  then we have to take all of them. Therefore we approximate them with
  the substitution that ensures that all values in $\tau$ may be a
  result of the application. That is
  \begin{enumerate}
    \item Find whether $\exists \sigma_\circ$ such that $\tau\leq t\sigma_\circ$ \beppe{does it always exists, and if not, what does it mean?}
    \item Specialize for the ``then'' branch, the type of $x_1$ and of $x_2$ by applying the substitution $\sigma_\circ$ to them.
  \end{enumerate}
\end{itemize}
For the ELSE branch we proceed as the above but considering the test $x_1x_2\in \neg\tau$.


In summary the algorithm is defined as follows:
\begin{description}
  \item [THEN branch:] $\exists \sigma$ such that $t\sigma\leq\neg\tau$?
     \begin{description}
        \item[no:] no specialization is possible
        \item[yes:] find  $\sigma_\circ$ such that $\tau\leq t\sigma_\circ$ and refine  in the ``then'' branch the type of $x_2$ as $u\sigma_\circ$, of $x_1$ as $s\sigma_\circ\to t\sigma_\circ$, and of $x_1x_2$ as $t\sigma_\circ$.
        \end{description}
   \item [ELSE branch:] $\exists \sigma$ such that $t\sigma\leq\tau$?
         \begin{description}
        \item[no:] no specialization is possible
        \item[yes:] find  $\sigma_\circ$ such that $\neg\tau\leq t\sigma_\circ$ and refine in the ``else'' branch  the type of $x_2$ as $u\sigma_\circ$, of $x_1$ as $s\sigma_\circ\to t\sigma_\circ$, and of $x_1x_2$ as $t\sigma_\circ$.
     \end{description}
\end{description}
Notice that on the given examples the algorithm returns the expected results \beppe{please check!}.


All the discussion we did above holds only when the type variables at issue are so-called \emph{monomorphic} type variables. These are variables that are bound somewhere else and all occurrences of which will be all instantiated with the same type. For instance if  $x_2$ is the polymorphic identity function we want to still use it polymorphically in the branches and specialize its type:
\[\begin{array}{{r}{l}}
  \texttt{let}&x_2\texttt{ = }\lambda x.x \\
  \texttt{ in}&\ite {(x_2 x_2}{\Int)}{x_2(\texttt{true})}{(x_2x_2)\texttt{false}}
  \end{array}
\]
rather than specializing it to either $\Int$ or $\Int\to\Int$ (or, worse since unsound, to their intersection).

In other terms we want to enrich occurrence typing by instantiating
types in a particular way, only if we know that those types will be
instantiated all in the same way.  For that we have to syntactically
distinguish polymorphic functions from monomorphic ones, so as to
deduce that $x_2$ in the example above has type
$\forall\alpha.\alpha\to\alpha$, rather than $\alpha\to\alpha$.
