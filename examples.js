"use strict";
var ENil;
(function (ENil) {
    ENil[ENil["NIL"] = 0] = "NIL";
})(ENil || (ENil = {}));
;
function isEmpty3(x) {
    if (x.kind == 1) {
        return true;
    }
    else if (x.kind == 2 && x.children == ENil.NIL) {
        return true;
    }
    else if (x.kind == 2) {
        return false;
    }
    else {
        return x.isEmpty();
    }
}
isEmpty3({ kind: 2, children: ENil.NIL });
function foo() {
    if (Math.random() < .5) {
        return ENil.NIL;
    }
    else {
        return { node: { kind: 1 }, next: ENil.NIL };
    }
}
isEmpty3({ kind: 2, children: foo() });
function or_(x, y) {
    return x || y;
}
var nn = or_(false, false);
function not_(x) {
    return !x;
}
function and_(x, y) {
    return x || y;
}
;
var l = and_(false, true);
console.log(l);
