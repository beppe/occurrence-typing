\rev{We present in this section preliminary results obtained by our
implementation. After giving some technical highlights, we focus on
demonstrating the behavior of our typing algorithm on meaningful
examples. We also provide an in-depth comparison with the fourteen
examples of \cite{THF10}}.

\subsection{Implementation details}

We have implemented the algorithmic system $\vdashA$ we presented in Section~\ref{sec:algorules}. Besides the type-checking
algorithm defined on the base language, our implementation supports
the record types and expressions of Section \ref{ssec:struct} and the refinement of
function types 
\iflongversion%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
described in Section \ref{sec:refining}. Furthermore, our implementation uses for the inference of arrow types
the following improved rule:
\input{optimize}
\else
of Section \ref{sec:refining} with the rule of
Appendix~\ref{app:optimize}.
\fi%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The implementation is rather crude and consists of 2000 lines of OCaml code,
including parsing, type-checking of programs, and pretty printing of
types. \rev{CDuce is used as a library to provide set-theoretic types and
semantic subtyping. The implementation faithfully transcribes in OCaml
the algorithmic system $\vdashA$ as well as all the type operations
defined in this work. One optimization that our implementation
features (with respect to the formal presentation) is the use of a
memoization environment in the code of the $\Refine {e,t}{\Gamma}$
function, which allows the inference to avoid unnecessary traversals
of $e$.
Lastly, while our prototype allows the user to specify a particular
value for the $n_o$ parameter we introduced in
Section~\ref{sec:typenv}, a value of $1$ for $n_o$ is sufficient
to check all examples we present in the rest of the section.
}

\subsection{Experiments}
We
demonstrate the output of our type-checking implementation in
Table~\ref{tab:implem} and Table~\ref{tab:implem2}. Table~\ref{tab:implem} lists
some examples, none of which can be typed by current systems. Even though some
systems such as Flow and TypeScript can type some of these examples by adding
explicit type annotations, the code 6, 7, 9, and 10 in Table~\ref{tab:implem}
and, even more,  the \code{and\_} and \code{xor\_} functions given
in \eqref{and+} and \eqref{xor+} later in this
section are out of reach of current systems, even when using the right explicit
annotations. 

It should be noted that for all the examples we present, the
time for the type inference process is less than 5ms, hence we do not report
precise timings in the table. These and other examples can be tested in the
online toplevel available at
\url{https://occtyping.github.io/}%
\input{code_table}

In Table~1, the second column gives a code fragment and the third
column the type deduced by our implementation as is (we
pretty printed it but we did not alter the output). Code~1 is a
straightforward function similar to our introductory
example \code{foo} in (\ref{foo}) and (\ref{foo2}) where \code{incr}
is the successor function and \code{lneg} the logical negation for
Booleans. Here the
programmer annotates the parameter of the function with a coarse type
$\Int\vee\Bool$. Our implementation first type-checks the body of the
function under this assumption, but doing so it collects that the type of
$\code{x}$ is specialized to \Int{} in the ``then'' case and to \Bool{}
in the ``else'' case. The function is thus type-checked twice more
under each hypothesis for \code{x}, yielding the precise type
$(\Int\to\Int)\land(\Bool\to\Bool)$. Note that w.r.t.\
rule \Rule{AbsInf+} of Section~\ref{sec:refining}, the
rule  \Rule{AbsInf++} we use in the  implementation improves the output of the computed
type. Indeed, using rule~[{\sc AbsInf}+] we would have obtained the
type
$(\Int\to\Int)\land(\Bool\to\Bool)\land(\Bool\vee\Int\to\Bool\vee\Int)$
with a redundant arrow. Here we can see that, since we deduced
the first two arrows $(\Int\to\Int)\land(\Bool\to\Bool)$, and since
the union of their domain exactly covers the domain of the third arrow, then
the latter is not needed. Code~2 shows what happens when the argument
of the function is left unannotated (i.e., it is annotated by the top
type \Any, written ``\code{Any}'' in our implementation). Here
type-checking and refinement also work as expected, but the function
only type checks if all cases for \code{x} are covered (which means
that the function must handle the case of inputs that are neither in \Int{}
nor in \Bool).

The following examples paint a more interesting picture. First
(Code~3) it is
easy in our formalism to program type predicates such as those
hard-coded in the $\lambda_{\textit{TR}}$ language of \citet{THF10}. Such type
predicates, which return \code{true} if and only if their input has
a particular type, are just plain functions with an intersection
type inferred by the system of Section~\ref{sec:refining}. We next define Boolean connectives as overloaded
functions. The \code{not\_} connective (Code~4) just tests whether its
argument is the Boolean \code{true} by testing that it belongs to
the singleton type \True{} (the type whose only value is
\code{true}) returning \code{false} for it and \code{true} for
any other value (recall that $\neg\True$ is equivalent to
$\texttt{Any\textbackslash}\True$). It works on values of any type,
but we could restrict it to Boolean values by simply annotating the
parameter by \Bool{} (which, in the CDuce's types that our system uses,  is syntactic sugar for
\True$\vee$\False) yielding the type
$(\True{\to}\False)\wedge(\False{\to}\True)$.
The \code{or\_} connective (Code~5) is straightforward as far as the
code goes, but we see that the overloaded type precisely captures all
possible cases: the function returns \code{false} if and only if both
arguments are of type $\neg\True$, that is, they are any value
different from \code{true}. Again we use a generalized version of the
\code{or\_} connective that accepts and treats any value that is not
\code{true} as \code{false} and again, we could easily restrict the
domain to \Bool{} if desired.\\
\indent
To showcase the power of our type system, and in particular of
the ``$\worra{}{}$''
type operator, we define \code{and\_} (Code~6) using De Morgan's
Laws instead of
using a direct definition. Here the application of the outermost \code{not\_} operator is checked against type \True. This
allows the system to deduce that the whole \code{or\_} application
has type \False, which in turn leads to \code{not\_\;x} and
\code{not\_\;y} to have type $\lnot \True$ and therefore both \code{x}
and \code{y} to have type \True. The whole function is typed with
the most precise type (we present the type as printed by our
implementation, but the first arrow of the resulting type is
equivalent to
$(\True\to\lnot\True\to\False)\land(\True\to\True\to\True)$).

All these type predicates and Boolean connectives can be used together
to write complex type tests, as in Code~7. Here we define a function
\code{f} that takes two arguments \code{x} and \code{y}. If
\code{x} is an integer and \code{y} a Boolean, then it returns the
integer \code{1}; if \code{x} is a character or
\code{y} is an integer, then it returns \code{2}; otherwise the
function returns \code{3}. Our system correctly deduces a (complex)
intersection type that covers all cases (plus several redundant arrow
types). That this type is as precise as possible can be shown by the fact that
when applying
\code{f} to arguments of the expected type, the \emph{type
statically deduced} for the
whole expression is the singleton type \code{1}, or \code{2},
or \code{3}, depending on the type of the arguments.

Code~8 allows us to demonstrate the use and typing of record paths. We
model, using open records, the type of DOM objects that represent XML
or HTML documents. Such objects possess a common field
\code{nodeType} containing an integer constant denoting the kind of
the node (e.g., \p{1} for an element node, \p{3} for a text node, \ldots). Depending on the kind, the object will have
different fields and methods. It is common practice to perform a test
on the value of the \code{nodeType} field. In dynamic languages such
as JavaScript, the relevant field can directly be accessed
after having checked for the appropriate \code{nodeType}, whereas
in statically typed languages such as Java, a downward cast
from the generic \code{Node} type to the expected precise type of
the object is needed. We can see that using the record expressions presented in
Section~\ref{ssec:struct} we can deduce the correct type for
\code{x} in all cases. Of particular interest is the last case,
since we use a type case to check the emptiness of the list of child
nodes. This splits, at the type level, the case for the \Keyw{Element}
type depending on whether the content of the \code{childNodes} field
is the empty list or not.

Code~9 shows the usefulness of the rule \Rule{OverApp}.
Consider the definition of the \code{xor\_} operator.
Here the rule~[{\sc AbsInf}+] is not sufficient to precisely type the
function, and using only this rule would yield a type
 $\Any\to\Any\to\Bool$.
\iflongversion
Let us follow the behavior of the
 ``$\worra{}{}$'' operator. Here the whole \code{and\_} is requested
 to have type \True, which implies that \code{or\_ x y} must have
 type \True. This can always happen, whether \code{x} is \True{} or
 not (but then depends on the type of \code{y}). The ``$\worra{}{}$''
 operator correctly computes that the type for \code{x} in the
 ``\emph{then}'' branch is $\True\vee\lnot\True\lor\True\simeq\Any$,
and a similar reasoning holds for \code{y}.
\fi%%%%%%%%%%%%%%
However, since \code{or\_} has type
%\\[.7mm]\centerline{%
$(\True\to\Any\to\True)\land(\Any\to\True\to\True)\land
   (\lnot\True\to\lnot\True\to\False)$
%}\\[.7mm]
then the rule \Rule{OverApp} applies and \True, \Any, and $\lnot\True$ become candidate types for
\code{x}, which allows us to deduce the precise type given in the table. Finally, thanks to rule \Rule{OverApp} it is not  necessary to use a type case to force refinement. As a consequence, we can define the functions \code{and\_} and \code{xor\_} more naturally as:
\begin{alltt}\color{darkblue}
  let and_ = fun (x : Any) -> fun (y : Any) -> not_ (or_ (not_ x) (not_ y)) \refstepcounter{equation}          \mbox{\color{black}\rm(\theequation)}\label{and+}
  let xor_ = fun (x : Any) -> fun (y : Any) -> and_ (or_ x y) (not_ (and_ x y)) \refstepcounter{equation}      \mbox{\color{black}\rm(\theequation)}\label{xor+}
\end{alltt}
for which the very same types as in Table~\ref{tab:implem} are deduced.

As for Code~10 (corresponding to our introductory
example~\eqref{nest1}), it illustrates the need for iterative refinement of
type environments, as defined in Section~\ref{sec:typenv}. As
explained, a single pass analysis would deduce
for {\tt x}
a type \Int{} from the {\tt f\;x} application and \Any{} from the {\tt g\;x}
application. Here by iterating a second time, the algorithm deduces
that {\tt x} has type $\Empty$ (i.e., $\textsf{Empty}$), that is, that the first branch can never
be selected (and our implementation warns the user accordingly). In hindsight, the only way for a well-typed overloaded function to have
type $(\Int{\to}\Int)\land(\Any{\to}\Bool)$ is to diverge when the
argument is of type \Int: since this intersection type states that
whenever the input is \Int, {\em both\/} branches can be selected,
yielding a result that is at the same time an integer and a Boolean.
This is precisely reflected by the case $\Int\to\Empty$ in the result.
Indeed our {\tt example10} function can be applied to an integer, but
at runtime the application of {\tt f\,x} will diverge.


Code~11 implements the typical type-switching pattern used in JavaScript. While
languages such as Scheme and Racket hard-code specific type predicates for each
type---predicates that our system does not need to hard-code since they can be
directly defined (cf. Code~3)---, JavaScript hard-codes a \code{typeof} function
that takes an expression and returns a string indicating the type of the
expression. Code~11 shows that \code{typeof} can be encoded and precisely typed
in our system. Indeed, constant strings are simply encoded as fixed list of
characters (themselves encoded as pairs as usual, with special atom \code{nil}
representing the empty list). Thanks to our precise tracking of singleton types
both in the result type of \code{typeof} and in the type case of
\code{test}, we can deduce for the latter a precise type (the given in
Table~\ref{tab:implem} is equivalent to
$(\textsf{Any}\to\Int)\wedge(\lnot(\Bool{\vee} \Int {\vee} \Char) \to 0)$).

Code~12 simulates the behavior of JavaScript property resolution, by looking
for a property \code{l} either in the object \code{o} itself or in the
chained list of its \code{prototype} objects. In this example, we first model
prototype-chaining by defining a type \code{Object} that can be either the
atom \code{Null} or any record with a \code{prototype} field which contains
(recursively) an \code{Object}. To ease the reading, we defined a recursive
type \code{ObjectWithPropertyL} which is either a record with a field
\code{l} or a record with a prototype of type \code{ObjectWithPropertyL}. We
can then define two predicate functions \code{has\_property\_l} and
\code{has\_own\_property\_l} that test whether an object has a property
through its prototype or directly. Lastly, we can define a function
\code{get\_property\_l} which directly accesses the field if it is present, or
recursively search for it through the prototype chain; the recursive
search is implemented by calling the explicitly-typed
parameter \code{self} which, in our syntax, refers to the function itself. Of
particular interest is the type deduced for the two predicate functions. Indeed,
we can see that \code{has\_own\_property\_l} is given an overloaded type whose
first argument is in each case a recursive record type that describes precisely
whether \code{l} is present at some point in the list or not (recall that
in a record type a field such as $\orecord{ \ell=?\textsf{Empty} }$, indicate that field \code{$\ell$}
is surely absent). 
Notice that in our language a fixpoint combinator can be defined as follows
\begin{alltt}\color{darkblue}
type X = X -> \textit{S} -> \textit{T}
let z = fun (((\textit{S} -> \textit{T}) -> \textit{S} -> \textit{T} ) -> (\textit{S} -> \textit{T})) f ->
      let delta = fun ( X -> (\textit{S} -> \textit{T}) ) x ->
         f ( fun (\textit{S} -> \textit{T}) v -> ( x x v ))
       in delta delta   
\end{alltt}
which applied to any function \code{f:(\textit{S}$\to$\textit{T})$\to$\textit{S}$\to$\textit{T}}
returns  a function \code{(z\,f):\textit{S}$\to$\textit{T}} such that
 for every non
diverging expression \code{$e$} of type \code{\textit{S}}, the
expression \code{(z\,f)$e$} (which is of type \code{\textit{T}}) reduces to \code{f((z\,f)$e$)}.
It is then clear that definition of \code{get\_property\_l} in Code 12, is nothing but syntactic sugar for
\begin{alltt}\color{darkblue}
let get_property_l =
  let aux = fun (self:Object->Any) -> fun (o:Object)->
    if has_own_property_l o is True then o.l
    else if o is Null then null
    else self (o.prototype)
  in z aux
\end{alltt}
where  \code{\textit{S}} is \code{Object} and  \code{\textit{T}} is \code{Any}.

\subsection{Comparison}

\input{code_table2}
In Table~\ref{tab:implem2}, we reproduce in our syntax the 14
archetypal examples of
\citet{THF10} (we tried to complete such examples with neutral code when they
were incomplete in the original paper). Of these 14 examples,
Example~1 to 13 depict combinations of type predicates (such
as \code{is\_int}) used either directly or through Boolean
predicates (such as the \code{or\_} function previously
defined). Note that for all examples for which there was no explicit
indication in the original version, we \emph{infer} the type of the
function whereas in~\cite{THF10} the same examples are always in a
context where the type of identifiers is known or the input type of
function is fully annotated.  Notice also that for Example~6, the goal
of the example is to show that indeed, the function is ill-typed
(which our typechecker detects accurately).



%% {\definecolor{lightgray}{gray}{.8}
%% \color{lightgray}OLD VERSION

%% The original Example~14
%% could be written in our syntax as:
%% \begin{alltt}\color{darkblue}
%%   let example14_orig = fun (input : Int | String) ->
%%     fun (extra : (Any, Any)) ->
%%       if (input, is_int (fst extra)) is (Int, True) then
%%          add input (fst extra)
%%       else if is_int (fst extra) is True then
%%          add (strlen input) (fst extra)
%%       else 0
%% \end{alltt}
%% Notice, in the version above the absence of an occurrence of \code{input} in
%%  the second \code{if} statement. Indeed, if the first test fails, it is either
%%  because \code{is\_int (fst extra)} is not \code{True} (i.e., if
%%  \code{fst extra} is not an integer) or because \code{input} is not an
%%  integer. Therefore, in our setting, the type information propagated to the
%%  second test is : $\code{(input, is\_int (fst extra))} \in \lnot (\Int,
%%  \True)$, that is $\code{(input, is\_int (fst extra))} \in (\lnot\Int,
%%  \True)\lor(\Int, \False)$. Therefore, the type deduced for \code{input} in the second
%%  branch is $(\String\lor\Int) \land (\lnot\Int \lor \Int) =
%%  \String\lor\Int$ which is not precise enough. By adding an occurrence of
%%  \code{input} in our \code{example14\_alt}, we can further restrict its type
%%  typecheck the function. Lifting this limitation through a control-flow analysis
%%  is part of our future work.
%% }

The original Example~14 of~\citet{THF10} is the only case of their
work that our
system cannot directly capture. It can be written in our syntax as:
\begin{alltt}\color{darkblue}
  let example14 = fun (input : Int|String) ->
    fun (extra : (Any, Any)) ->
      if and2_(is_int input , is_int(fst extra)) is True then
         add input (fst extra)  \refstepcounter{equation}                                                     \mbox{\color{black}\rm(\theequation)}\label{ex14}
      else if is_int(fst extra) is True then
         add (strlen input) (fst extra)
      else 0
\end{alltt}
where \code{and2\_} is the uncurried version of the \code{and\_}
function we defined in \eqref{and+} and \code{is\_int} is the function
defined in the third row of Table~\ref{tab:implem}.
Our system rejects the expression above, while the system by~\citet{THF10}
correctly infers the function always return an integer. The reason
why our system rejects it is because the type it deduces for the
occurrence of \code{input} in the 6th line of the code
is \code{Int|String} rather than \code{String} as required by the
application of \code{strlen}. The general reason for this failure is
that, contrary to~\cite{THF10}, our system does not implement an
analysis of the flow of type information. In particular, since the
variable \code{input} does not occur in the condition of the
second \code{if}, then its type is not refined (as it could be).
Indeed, if the first test fails, it is either
because  \code{fst\,extra} is not an integer
(i.e., \code{is\_int(fst\,extra)} is not \True) or
because \code{input} is not an integer. Therefore, in
our setting, the type information propagated to the second test for
the pair of the arguments in the first test is :
$\code{(is\_int input , is\_int(fst\,extra))} \in \lnot (\True, \True)$, that
is $\code{(input, is\_int(fst\,extra))} \in
(\lnot\Int, \True)\lor(\Int, \False)$. Since the second test checks
whether \code{is\_int(fst\,extra)} holds or not, then we could deduce
that the following occurrence of \code{input} is of type
$\lnot\Int$. But since \code{input} does not occur in the test, this
refinement of the type of \code{input} is not done. Instead, the type deduced
for \code{input} in the second branch is $(\String\lor\Int) \land
(\lnot\Int \lor \Int) = \String\lor\Int$ which is not precise
enough to type the application \code{strlen\;input}. It not difficult
to patch, alas unsatisfactorly, this example in our system: it suffices to test
dummily in the second \code{if} the whole argument of \code{and2\_},
without really checking its first component:
\begin{alltt}\color{darkblue}
  let example14_alt = fun (input : Int|String) ->
    fun (extra : (Any, Any)) ->
      if and2_(is_int input , is_int(fst extra)) is True then
         add input (fst extra)
      else if (is_int input , is_int(fst extra)) is (Any,True) then
         add (strlen input) (fst extra)
      else 0
\end{alltt}
Even if the type of \code{is\_int\,input} is not really tested (any
result will produce the same effect) its presence in the test triggers the
refinement of the type of the last occurrence of \code{input}, which
type checks with the (quite precise) type shown in the entry 14 of
Table~\ref{tab:implem2}, type that is equivalent to $\Int\vee\String \to ((\Int,\textsf{Any}) \to \Int) \wedge ((\lnot\Int,\textsf{Any}) \to 0)$.
Lifting this limitation through a control-flow
analysis is part of our future work.

\rev{%%%
In our system, however, it is possible to express dependencies between
different arguments of a function by uncurrying the function and typing its
arguments by a union of products. To understand this point, consider this
simple example:}
\begin{alltt}\color{darkblue}
  let sum = fun (x : Int|String) -> fun (y : Int|String) ->
      if x is String then concat x y else add x y
\end{alltt}
\rev{%%%
The definition above does not type-check in any available system, and rightly
does so since nothing ensures
that \code{x} and \code{y} will be either both strings (so
that \code{concat} does not fail) or both integers (so that \code{add}
does not fail). It is however possible to state this dependency
between the type of the two arguments by
uncurring the function and using a union type:
}%%%rev
\begin{alltt}\color{darkblue}
  let sum = fun (x : (Int,Int)|(String,String))
      if fst x is String then concat(fst x)(snd x) else add(fst x)(snd x)
\end{alltt}
\rev{%%%
this function type-checks in our system (and, of course, in Typed
Racket as well) but the corresponding type-annotated version in JavaScript
}%%%rev
\begin{alltt}\color{darkblue}
  function sum (x : [string,string]|[number,number]) \{
    if (typeof x[0] === "string") \{
       return x[0].concat(x[1]); 
    \} else \{
       return x[0] + x[1];
    \}
  \}
\end{alltt}
\rev{%%%%
is rejected both by Flow and TypeScript since their type analyses fail to detect the
dependency of the types of the two projections.
}%%%rev

Although these experiments are still preliminary, they show how the
combination of occurrence typing and set-theoretic types, together
with the type inference for overloaded function types presented in
Section~\ref{sec:refining} goes beyond what languages like
TypeScript and Flow do, since they can only infer single arrow types.
Our refining of overloaded
functions is also future-proof and resilient to extensions: since it ``retypes'' functions
 using information gathered by the typing of occurrences in the body,
 its precision will improve with any improvement of
 our occurrence typing framework.
