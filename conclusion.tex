In this work we presented the core of our analysis of occurrence
typing, extended it to record types and proposed a couple of novel
applications of the theory, namely the reconstruction of
intersection types for unannotated functions and a static analysis to reduce the number of
casts inserted when compiling gradually-typed programs.
One of the by-products of our work is the ability to define type
predicates such as those used in \cite{THF10} as plain functions and
have the inference procedure deduce automatically the correct
overloaded function type. More generally, our approach surpasses 
current ones in that it can deduce precise (overloaded) types for
functions that in all other approaches either require 
the programmer to specify the full precise type (e.g., the
function \code{foo} we defined in \eqref{foo} and \eqref{foo2}  in
our introduction) or cannot be typed at all (the \code{and\_} and \code{xor\_} functions given
in \eqref{and+} and \eqref{xor+} are the most eloquent examples).

There is still a lot of work to do to fill the gap with real-world
programming languages. For example, our analysis cannot handle flow of
information, as we discussed for the function \code{example14} in Section~\ref{sec:practical}.
In particular, the result of a type test can flow only to the branches
but not outside the test. As a consequence the current system cannot
type a let binding such as \code{
%\begin{alltt}\color{darkblue}
  let x = (y\(\in\)Int)?{`}yes:`no in (x\(\in\)`yes)?y+1:not(y)%
%\end{alltt}
}
which is clearly safe when  $y:\Int\vee\Bool$. Nor can this example be solved by partial evaluation since we do not handle nesting of tests in the condition\code{(\,((y\(\in\)Int)?{`}yes:`no)\(\in\)`yes\,)\,?\,y+1\,:\,not(y)},
and both are issues that the system by~\citet{THF10} can handle. We think that it is possible
to reuse some of their ideas to perform an information flow analysis on top of
our system to remove these limitations.
%
\iflongversion%
Some of the extensions we hinted to in Section~\ref{sec:practical}
warrant a formal treatment. In particular, the rule [{\sc OverApp}]
only detects the application of an overloaded function once, when
type-checking the body of the function against the coarse input type
(i.e., $\psi$ is computed only once). But we could repeat this
process whilst type-checking the inferred arrows (i.e., we would
enrich $\psi$ while using it to find the various arrow types of the
lambda abstraction). Clearly, if untamed, such a process may never
reach a fix point. Studying whether this iterative refining can be
made to converge and, foremost, whether it is of use in practice is among our objectives.
\fi%

But the real challenges that lie ahead are the handling of side
effects and the addition of polymorphic types.
\rev{%%%
Our analysis works in 
pure languages and we already discussed at length at the end of the
previous section our plans to extend it to
cope with side-effects. However, the
ultimate solution of integrating type and effect analysis in a unique tool
is not more defined than that.
}%%%
For polymorphism, instead, we can easily adapt
the main idea of this work to the polymorphic setting. Indeed, the
main idea  is to remove from the type of an expression all
the results of the expression that would make some test fail (or
succeed, if we are typing a negative branch). This is done by
applying an intersection to the type of the expression, so as to keep
only the values that may yield success (or failure) of the test. For
polymorphism the idea is the same, with the only difference that
besides applying an intersection we can also apply an
instantiation. The idea is to single out the two most general type
substitutions for which some test may succeed and fail, respectively, and apply these
substitutions to refine the types of the corresponding occurrences
in the ``then'' and ``else'' branches. Concretely, consider the test
$x_1x_2\in t^\circ$ where $t^\circ$ is a closed type and $x_1$, $x_2$ are
variables of type $x_1: s\to t$ and $x_2: u$ with $u\leq s$. For the
positive branch we first check whether there exists a type
substitution $\sigma$ such that $t\sigma\leq\neg t^\circ$. If it does not
exists, then this means that for all possible assignments of
polymorphic type variables of $s\to t$, the test may succeed, that is,
the success of the test does not depend on the particular instance of
$s\to t$ and, thus, it is not possible to pick some substitution for
refining the occurrence typing. If it exists, then 
we find a type substitution $\sigma_\circ$ such that $t^\circ\leq
t\sigma_\circ$ and we refine for the
positive branch the types of $x_1$, of $x_2$, and of $x_1x_2$ by applying $\sigma_\circ$ to their types. While the
idea is clear%
\iflongversion%%%%%%%%%%%%%%%%%%%
,
\else%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\ (see \Appendix\ref{app:roadmap} for a more detailed explanation),
\fi%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
the technical details are quite involved, especially if we also want
functions with intersection types and/or  gradual
typing. Nevertheless, our approach has an edge on systems that do not
account for polymorphism.
\iflongversion
This needs a whole gamut of non trivial research that we plan to
develop in the near future building on the work on polymorphic types
for semantic subtyping~\cite{CX11} and the research on the definition of
polymorphic languages with set-theoretic types
by~\citet{polyduce2,polyduce1,CPN16} and \citet{Pet19phd}.
\fi
