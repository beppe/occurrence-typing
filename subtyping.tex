\noindent
Subtyping is defined by giving a set-theoretic interpretation of the
types of Definition~\ref{def:types} into a suitable domain $\Domain$: 
\begin{definition}[Interpretation domain~\cite{Frisch2008}]\label{def:interpretation}
The \emph{interpretation domain} $ \Domain $ is the set of finite terms $ d $
produced inductively by the following grammar\vspace{-2mm}
\begin{align*}
  d & \Coloneqq  c \mid (d, d) \mid \Set{(d, \domega), \dots, (d, \domega)}
    \\
    \domega & \Coloneqq d \mid \Omega
\end{align*}
where $ c $ ranges over the set $ \Constants $ of constants
and where $ \Omega $ is such that $ \Omega \notin \Domain $.
\end{definition}
The elements of $ \Domain $ correspond, intuitively,
to (denotations of) the results of the evaluation of expressions.
In particular, in a higher-order language,
the results of  computations can be functions which, in this model,
are represented by sets of finite relations
of the form $ \Set{(d_1, \domega_1), \dots, (d_n, \domega_n)} $,
where $ \Omega $ (which is  not in $ \Domain $)
can appear in second components to signify
that the function fails (i.e., evaluation is stuck) on the
corresponding input. This is implemented by using in the second
projection the meta-variable $\domega$ which ranges over
$ \Domain_\Omega = \Domain \cup \Set{\Omega} $ (we reserve
$d$ to range over $\Domain$, thus excluding $\Omega$).
This constant $\Omega$ is used to ensure that $\Any\to\Any$ is not a
supertype of all function types: if we used $d$ instead of $\domega$,
then every well-typed function could be subsumed to  $\Any\to\Any$
and, therefore, every application could be given the type $\Any$,
independently from its argument as long as this argument is typable (see Section 4.2 of~\cite{Frisch2008} for details).
The restriction to \emph{finite} relations corresponds to the intuition
that the denotational semantics of a function is given by the set of
its finite approximations, where finiteness is a restriction necessary
(for cardinality reasons) to give the
semantics to higher-order functions.

We define the interpretation $ \TypeInter{t} $ of a type $ t $
so that it satisfies the following equalities,
where  $ \Pf $ denotes the restriction of the powerset to finite
subsets and $  \ConstantsInBasicType{}$ denotes the function
that assigns to each basic type the set of constants of that type, so
  that  for every constant $c$ we have $
  c\in \ConstantsInBasicType(\basic {c})$
\ifelsevierstyle
(we use $\basic{c}$ to denote the
basic type of the constant $c$):
\else
($\basic c$ is
  defined in Section~\ref{sec:syntax}):
\fi
\begin{align*}
  \TypeInter{\Empty} & = \emptyset&
  \TypeInter{t_1 \lor t_2} & = \TypeInter{t_1} \cup \TypeInter{t_2} &
  \TypeInter{\lnot t} & = \Domain \setminus \TypeInter{t} 
  \\
  \TypeInter{b} & = \ConstantsInBasicType(b) &
  \TypeInter{t_1 \times t_2} & = \TypeInter{t_1} \times \TypeInter{t_2} \\
  \TypeInter{t_1 {\to} t_2} & =
    \{R \in \Pf(\Domain{\times}\Domain_\Omega) \mid\forall (d, \domega) \in R. \:d \in \TypeInter{t_1} \implies \domega \in \TypeInter{t_2}\}
   \span\span
   \span\span
\end{align*}
We cannot take the equations above
directly as an inductive definition of $ \TypeInter{} $
because types are not defined inductively but coinductively.
\iflongversion%%%%%%%%%%%%%%%%%%%
However, recall that the contractivity condition of
Definition~\ref{def:types}  ensures that the binary relation $\vartriangleright
\,\subseteq\!\types{\times}\types$ defined by $t_1 \lor t_2 \vartriangleright
t_i$, $t_1 \land t_2 \vartriangleright t_i$, $\neg t \vartriangleright t$ is Noetherian which gives an induction principle  on $\types$ that we
use combined with  structural induction on $\Domain$ to give the
following definition
which validates these equalities.
\else
Notice however that the contractivity condition of
Definition~\ref{def:types}  ensures that the binary relation $\vartriangleright
\,\subseteq\!\types{\times}\types$ defined by $t_1 \lor t_2 \vartriangleright
t_i$, $t_1 \land t_2 \vartriangleright
t_i$, $\neg t \vartriangleright t$ is Noetherian.
This gives an induction principle\footnote{In a nutshell, we can do
proofs and give definitions by induction on the structure of unions and negations---and, thus, intersections---but arrows, products, and basic types are the base cases for the induction.}  on $\types$ that we
use combined with  structural induction on $\Domain$ to give the following definition,
which validates these equalities.
\fi%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{definition}[Set-theoretic interpretation of types~\cite{Frisch2008}]\label{def:interpretation-of-types}
We define a binary predicate $ (d : t) $
(``the element $ d $ belongs to the type $t$''),
where $ d \in \Domain $ and $ t \in \types $,
by induction on the pair $ (d, t) $ ordered lexicographically.
The predicate is defined as follows:
\begin{align*}
  (c : b)  &= c \in \ConstantsInBasicType(b) \\
  ((d_1, d_2) : t_1 \times t_2 )  &=
    (d_1 : t_1) \mathrel{\mathsf{and}} (d_2 : t_2) \\
  (\Set{(d_1, \domega_1),..., (d_n, \domega_n)} : t_1 \to t_2)  &=
   \forall i \in [1.. n] . \:
    \mathsf{if} \: (d_i : t_1) \mathrel{\mathsf{then}} (\domega_i : t_2) \\
  (d : t_1 \lor t_2)  &= (d : t_1) \mathrel{\mathsf{or}} (d : t_2) \\
  (d : \lnot t)  &= \mathsf{not} \: (d : t) \\
  (\domega : t)  &= \mathsf{false} & \text{ otherwise}
  \end{align*}
We define the \emph{set-theoretic interpretation}
$ \TypeInter{} : \types \to \Pd(\Domain) $
as $ \TypeInter{t} = \{d \in \Domain \mid (d : t)\} $.
\end{definition}
Finally,
we define the subtyping preorder and its associated equivalence relation
as follows.

\begin{definition}[Subtyping relation~\cite{Frisch2008}]\label{def:subtyping}
  We define the \emph{subtyping} relation $ \leq $
  and the \emph{subtyping equivalence} relation $ \simeq $
  as
  \(
    t_1 \leq t_2 \iffdef \TypeInter{t_1} \subseteq \TypeInter{t_2}\) and   
  \(t_1 \simeq t_2 \iffdef (t_1 \leq t_2) \mathrel{\mathsf{and}} (t_2 \leq t_1)
    \: .
  \)
\end{definition}
